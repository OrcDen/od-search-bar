import { PolymerElement, html } from '@polymer/polymer';
import '@webcomponents/shadycss/entrypoints/apply-shim.js';

export class OdSearchBar extends PolymerElement {
    static get is() { return 'od-search-bar' }
    static get template () {
        return html`
            <style>
                :host {
                    display: block;
                }

                #search-container {
                    display: flex;
                    width: 100%;
                    margin-bottom: .5em;
                }

                #search-input-box {
                    display: block;
                    width: 100%;
                    border: 1px solid #d2d2d2;
                    color: #aaa9a9;
                    background-color: #ffffff;
                    height: 40px;
                    padding: 15px;
                    box-sizing: border-box;
                }

                #clear-icon {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    min-width: 40px;
                    color: white;
                    background: black;
                    cursor: pointer;
                    transition: background .2s ease-in-out;
                    -webkit-transition: background .2s ease-in-out;
                    /* Safari */
                }

                #clear-icon:hover {
                    background: #304a89;
                }

                .link {
                    color: #009688;
                    -webkit-transition: color .2s ease-in-out;
                    /* Safari */
                    transition: color .2s ease-in-out;
                }

                .link:hover {
                    color: #304a89;
                    cursor: pointer;
                }

                #title::slotted(*) {
                    display: block;
                    font-size: 3em;
                    font-family: "Open Sans Condensed", Arial, Helvetica, sans-serif;
                    font-weight: 600;
                    line-height: 1.41;
                    letter-spacing: 0px;
                }
            </style>

            <slot name="search-title" id="title"></slot>
            <div id="search-container">
                <input id="search-input-box" type="text" on-input="updateSearch" placeholder="[[placeholder]]">
                <div id="clear-icon" on-click="clearInput">✖</div>
            </div>
        `;
    }
    static get properties() {
        return {
            searchString: {
                type: String,
                value: '',
                notify: true
            },

            placeholder: {
                type: String,
                value: ''
            }
        }
    }

    clearInput() {
        this.shadowRoot.querySelector( '#search-input-box' ).value = '';
        this.updateSearch();
    }

    updateSearch( inputEvent ) {
        var paramString = '';
        var search = this.shadowRoot.querySelector( '#search-input-box' ).value;
        if ( search !== null && search !== undefined && search !== '' ) {
            paramString = "?search=" + search;
        }
        this.searchString = paramString;
    }
}
customElements.define( OdSearchBar.is, OdSearchBar );